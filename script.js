"use strict";

let num;

while (num == '' || isNaN(num)) {
    num = prompt("Please enter an integer number", num);
}

function factorial(num) {
    if (num === 0) {
        return 1;
    } else {
        return num * factorial(num - 1);
    }
}

console.log(factorial(parseInt(num)));

